<?php

namespace lylib\captcha;

use rap\cache\Cache;

abstract class IVerify
{

    /**
     * 构造验证器
     * @param int $_sid
     * @return mixed
     */
    public abstract function build($_sid = 1);

    /**
     * 校验结果
     * @param $verify_code
     * @param $sid
     * @param string $_did
     * @param false $sense
     * @param string $verify_check
     * @return mixed
     */
    public abstract function check($verify_code,$sid,$_did='',$sense=false,$verify_check='');

    /**
     * @param bool $success
     * @param $type
     * @param string $_did 设备id
     * @param false $sense 是否开启无感验证
     * @param string $verify_check 参数
     * @return array|false[]
     */
    public static function checkResult(bool $success, $type, $_did='',$sense=false,$verify_check=''): array
    {
        if(!$success){
            return ['success' => false];
        }
        $session_id = request()->session()->sessionId();
        $token_key = md5($session_id . uniqid(mt_rand(), true));
        $token_check = md5(uniqid(mt_rand() . $session_id, true));
        $verify_check=EncryptUtil::decrypt($verify_check,$_did);
        $verify_check = substr($verify_check,16,strlen($verify_check)-16);
        $ip= request()->ip();
        $ua=request()->header('user-anget');
        $val = [
            'token_check' => $token_check,
            'ip' => request()->ip(),
            'session_id' => $session_id,
            'verify_check'=>$verify_check,
            'did'=>$_did,
            'type' => $type,
            'check_time' => time()
        ];
        Cache::set($token_key, $val, 60 * 5);
        $token=$token . $token_check;
        if($sense){
            $verify=md5($verify_check.$_did.$ip.$ua);
            Cache::set($token, verify, 60 * 10);
        }
        return ['success' => true, 'msg' => '恭喜你,验证成功', 'token' => $token];

    }

}