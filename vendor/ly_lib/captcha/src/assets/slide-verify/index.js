(function () {
    var panelHtml = '<div class="lyz-box"><div style="position: relative"><img class="lyz-box-img-bg"/><img class="lyz-box-item"/><div class="lyz-box-tool"><div class="lyz-box-refresh"></div></div></div><div class="lyz-move-box"><div class="lyz-moved"></div><div class="lyz-move-ctrl" style="left: -1px"><span class="lyz-move-icon"></span></div><span class="lyz-move-text">滑动滑块完成拼图</span></div></div>';
    var $panel=null;
    var config = {
        codeUrl: '/home/testcode',
        checkUrl: '/home/testcheck',
        verifyFail: function () {

        }, verifySuccess: function () {

        }
    }
    window.LyVerify = {
        slide: function (conf) {
            $.extend(config, conf);

            if(window.innerWidth>600){
                var $el=$(conf.el);
                var offset= $el.offset();
                var top=offset.top-110;
                var left=offset.left-60;
                if(top<10){
                    top=10;
                }
                if(left<10){
                    left=10;
                }
                if(left+310>window.innerWidth){
                    left=window.innerWidth-320;
                }
                $panel.css({
                    top:top,
                    left:left
                })
            }
            $panel.fadeIn();

        }
    }

    function init() {
        $panel = $(panelHtml);
        $(document.body).append($panel);
        var sign_key = '';
        var itemMd5 = '';
        var begin_time = 0;
        var $ctrl = $panel.find('.lyz-move-ctrl');
        var $moved = $panel.find('.lyz-moved');
        var $box = $panel.find('.lyz-move-box');
        var $item = $panel.find('.lyz-box-item');
        var $img = $panel.find('.lyz-box-img-bg');

        var $refresh = $panel.find('.lyz-box-refresh');
        $refresh.on('click', function () {
            loadVerify();
        });

        function mouseUp() {
            $ctrl.unbind('mousemove');
            var time = new Date().getTime() + '';
            var code = a(itemMd5 + time + $item.position().left + '', sign_key);
            var params = {};
            if (config.verifyCheckParams) {
                params = config.verifyCheckParams();
            }
            params = $.extend(params, {verify_code: code});
            $.post(config.checkUrl, params, function (rs) {
                $box.removeClass('lyz-moving');
                if (!rs.success) {
                    if (config.verifyFail) {
                        config.verifyFail(rs);
                    }

                    var animTime = 500;
                    $box.addClass('lyz-move-error');
                    $ctrl.animate({left: "-1px"}, animTime);
                    $moved.animate({'width': '0'}, animTime);
                    $item.animate({left: 0}, animTime);
                    setTimeout(function () {
                        $box.removeClass('lyz-move-error');
                        loadVerify();
                    }, animTime);
                } else {
                    if (config.verifySuccess) {
                        config.verifySuccess(rs);
                    }
                    $box.addClass('lyz-move-success');
                    setTimeout(function (){
                        $panel.fadeOut();
                    },1000);
                }
            });
        }

        function loadVerify() {
            $.get(config.codeUrl, function (rs) {
                var code = rs.code;
                var kv = code.split(' ');
                sign_key = kv[0].substr(0, 16) + kv[1].substr(0, 16);
                $img.attr('src', b(kv[0].substr(16), sign_key));
                $item.attr('src', b(kv[1].substr(16), sign_key));
                var y = kv[2].substr(32);
                itemMd5 = kv[2].substr(0, 32);

                $item.css({top: y + 'px'})
                begin_time = new Date().getTime();
            });
        }

        loadVerify();
        $ctrl.mousedown(function (e) {
            // 鼠标按下后就要取到当前元素的x、y的位置数据
            var x = e.pageX;
            // 取到了x和y的坐标对象
            var divx = $ctrl.position().left;
            $box.addClass('lyz-moving')
            // 绑定一个鼠标移动的事件
            $ctrl.bind('mousemove', function (event) {
                // 鼠标移动时 取当前移动后的x、y的数据
                var x2 = event.pageX;

                console.log(x2 - x);
                var left = divx + (x2 - x);
                if (left < -1) {
                    left = -1;
                }
                if (left > 246) {
                    left = 246;
                }
                $item.css({left: (left + 1) + 'px'});
                $moved.css('width', (left + 1) + 'px')
                $ctrl.css({
                    left: left + "px"
                });
            });
            // $ctrl.mouseleave(mouseUp);
        });

        // 当鼠标按钮抬起 放开的时候 把鼠标移动事件移除
        $ctrl.mouseup(mouseUp);
    }
    init();

    function a(str, sign_key) {
        var sign_b = c(sign_key);
        var str_b = c(str);
        var b = [];
        for (var i = 0; i < str_b.length; i++) {
            var k = str_b[i] ^ sign_b[i % sign_b.length];
            b.push(k);
        }
        return window.btoa(d(b));
    }

    function b(str, sign_key) {
        str = window.atob(str);
        var sign_b = c(sign_key);
        var str_b = c(str);
        var b = [];
        for (var i = 0; i < str_b.length; i++) {
            var k = str_b[i] ^ sign_b[i % sign_b.length];
            b.push(k);
        }
        return d(b);
    }


    function c(str) {
        var bytes = [];
        var len, c;
        len = str.length;
        for (var i = 0; i < len; i++) {
            c = str.charCodeAt(i);
            if (c >= 0x010000 && c <= 0x10FFFF) {
                bytes.push(((c >> 18) & 0x07) | 0xF0);
                bytes.push(((c >> 12) & 0x3F) | 0x80);
                bytes.push(((c >> 6) & 0x3F) | 0x80);
                bytes.push((c & 0x3F) | 0x80);
            } else if (c >= 0x000800 && c <= 0x00FFFF) {
                bytes.push(((c >> 12) & 0x0F) | 0xE0);
                bytes.push(((c >> 6) & 0x3F) | 0x80);
                bytes.push((c & 0x3F) | 0x80);
            } else if (c >= 0x000080 && c <= 0x0007FF) {
                bytes.push(((c >> 6) & 0x1F) | 0xC0);
                bytes.push((c & 0x3F) | 0x80);
            } else {
                bytes.push(c & 0xFF);
            }
        }
        return bytes;
    }

    function d(arr) {
        if (typeof arr === 'string') {
            return arr;
        }
        var str = '',
            _arr = arr;
        for (var i = 0; i < _arr.length; i++) {
            var one = _arr[i].toString(2),
                v = one.match(/^1+?(?=0)/);
            if (v && one.length === 8) {
                var bytesLength = v[0].length;
                var store = _arr[i].toString(2).slice(7 - bytesLength);
                for (var st = 1; st < bytesLength; st++) {
                    store += _arr[st + i].toString(2).slice(2);
                }
                str += String.fromCharCode(parseInt(store, 2));
                i += bytesLength - 1;
            } else {
                str += String.fromCharCode(_arr[i]);
            }
        }
        return str;
    }

})()