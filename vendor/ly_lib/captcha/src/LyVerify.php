<?php

namespace lylib\captcha;

use rap\cache\Cache;

class LyVerify
{
    public static function checkToken($token)
    {


        $mid = substr($token, 0, 32);
        $token_check = substr($token, 32);
        $val = Cache::get($mid);
        if ($val['token_check'] == $token_check) {
            unset($val['token_check']);
            $val['params']= json_decode(urldecode(EncryptUtil::decrypt($val['verify_check'],$val['did'])),true);
            return $val;
        }
        return [];
    }

    /**
     * @param bool $success
     * @param $type
     * @param string $_did 设备id
     * @param false $sense 是否开启无感验证
     * @param string $verify_check 参数
     * @return array|false[]
     */
    public static function checkResult(bool $success, $type, $_did = '', $sense = false, $verify_check = ''): array
    {
        if (!$success) {
            return ['success' => false];
        }
        $session_id = request()->session()->sessionId();
        $token_key = md5($session_id . uniqid(mt_rand(), true));
        $token_check = md5(uniqid(mt_rand() . $session_id, true));
        $verify_check = substr($verify_check, 16, strlen($verify_check) - 32);
        $ip = request()->ip();
        $ua = request()->header('user-agent');
        $val = [
            'token_check' => $token_check,
            'ip' => $ip,
            'session_id' => $session_id,
            'verify_check' => $verify_check,
            'did' => $_did,
            'type' => $type,
            'check_time' => time()
        ];
        Cache::set($token_key, $val, 60 * 5);
        $token = $token_key . $token_check;
        if ($sense) {
            $verify = md5($verify_check . $_did. $ua);
            Cache::set($token, $verify, 60 * 60*24*15);
        }
        return ['success' => true, 'msg' => '恭喜你,验证成功', 'token' => $token];
    }

    public static function senseCheck($token, $_did = '', $verify_check = '')
    {
        $val = Cache::get($token);
        if (!$val) {
            return ['success' => false];
        }
        $ip = request()->ip();
        $ua = request()->header('user-agent');
        $verify_check = substr($verify_check, 16, strlen($verify_check) - 32);
        $verify = md5($verify_check . $_did  . $ua);
        Cache::remove($token);
        Cache::remove(substr($token,0,32));
        $is_success = $verify == $val;
        if ($is_success) {
            $session_id = request()->session()->sessionId();
            $token_key = md5($session_id . uniqid(mt_rand(), true));
            $token_check = md5(uniqid(mt_rand() . $session_id, true));
            $val = [
                'token_check' => $token_check,
                'ip' => $ip,
                'session_id' => $session_id,
                'verify_check' => $verify_check,
                'did' => $_did,
                'type' => 'sense',
                'check_time' => time()
            ];
            Cache::set($token_key, $val, 60*5);
            $token = $token_key . $token_check;
            Cache::set($token, $verify, 60*60*24*15);
            return ['success' => true, 'token' => $token];
        }
        return ['success' => false];
    }

}