
/**
 * 生成模拟数据
 */
Rap.define('/table/mock.js', [], function () {
    var id = parseInt(Math.random() * 1000000);
    var MockData = {
        "role_name": ["高级管理员", "普通管理员", "财务", "老师", "课程顾问", "教务专员", "销售员", "市场主管", "助教"],
        "nickname": ["六六.大美宿州", "徐熊熊", "小马哥", "掌上洛南", "句号", "神木365", "云上玉溪", "李白楊-", "周源", "在长垣", "linhuilh", "江油都市网", "dayurrrr", "sdk", "致在必得", "H", "灿烂的阿猛", "Gaoyoungor", "刘胖子", "差点是个山大王", "Aa刀呀～", "十七 。", "tyuu", "时光不缓_", "大鱼", "啦啦啦", "宏宇", "欧阳海斌", "掌上平邑", "虚伪的世界", "北方狼人", "柚夏", "凯蓝", "集锦", "霸气男人", "jiawenkiss", "汽泡", "小枫晓语", "7106", "火星水怪", "隆回圈", "黎明前的黑暗。", "防腐圈", "kele2008", "mizzle", "运动三亚app 陈靖", "Ezi", "小谢总?微帝国CEO", "chengrs 11", "静水听风", "babybaby", "初心", "小穆", "巴画无钱", "小豆子", "钰酱", "世界", "支援", "庞公子?", "小米", "林西微生活-小编3", "阿猛", "逐梦到天涯", "剑门易", "百毒不侵", "Make", "villian", "香烟爱上火柴", "马小天", "那些年", "松果粑粑", "黑墙 | 半城云®", "上蔡零距离『翟启明』", "初", "疯子  fred", "小太阳☀", "FLORA佛拉", "魏巍", "喻晓松", "bo.", "Xiao Bai", "曾经牛过", "元鹏", "陈炼", "自定义", "胖掌柜", "狄坚", "战国狂", "风云闲人", "胖英熊", "黑墙", "北城网", "农民工", "陈兵", "合纵王锜", "孟凡超", "伊犁生", "标海豹", "伊犁生活网 张荣亮", "吕文涛", "沐心慕廢", "bshdb", "刘旺", "张扬", "那个唐", "乔松松", "心口窝", "百米哥", "赤兔非兔", "一亩孔洲", "呆ing", "橘子味的狗", "小小小心", "小琦", "Errol", "佛拉拉拉拉", "叶宇华么么哒", "kimsenter", "宝太灿", "现代标本", "hooyo", "yin007", "czz1226", "黄昏使者", "小阳先生", "张锋", "看看看", "NeVeRfOrGiVe", "·今ㄨī是何年", "bseric", "plecg", "潘潘-郫都微生活", "被强煎的蛋", "gh1019", "绘世界张光辉", "小海", "MAGAPP 果果", "黑子", "聚离", "陈鲁笙 MAGAPP&多云", "黄桥在线", "brownienie", "自强不息、在这聚合离散的流年", "男二号", "马上", "七月", "桃纸熊", "23：57", "_武", "Lie to me.", "冯海洋", "Don't worry be happy", "?happy?", "RunRun.Leon", "卜一", "Sweetnienie", "胜利社区", "温润如岚（蒙山网）", "眼巴巴", "A 习水生活网-宏宇", "18948779190", "后援会", "富平网", "addney", "Lily", "无痕", "溧水114网曹礼彬", "MAGAPP技术", "好孕窝网", "15nong", "王波", "珠宝窝", "sita", "深度日本俱乐部", "拉不拉狗狗", "dexiang", "封真"],
        "name": ["王豪", "凯蓝", "王凌涛", "邹隆江", "薛原", "黎平", "游承来", "胡陈", "陆昌洪", "万千里", "李健", "曾文明", "王程程", "汪浩", "王维", "余云贵", "冯平", "张雨白", "景颢", "杨世友", "普孟春", "王凯", "潘兴勇", "余伟", "罗晓南", "沈凤泽", "涂伟", "何强", "李伟", "郭强文", "隆戴立", "刘华春", "廖羿", "余腾飞", "罗毅", "孙晟磊", "唐飞", "李卫东", "冉卫东", "秦燕", "宋歌", "黄家强", "陈松", "刘君", "杜伟", "徐飞", "邹波", "王子", "张松", "罗科", "钟东", "万帆", "朗朗", "张良学", "伍海洋", "波哥", "谭绍东", "陈鹏", "陈磊", "赵礼", "陈庆", "兰时海", "蒋勇", "熊伟", "代波", "张波", "杨闯", "张奎", "江荣", "李军", "胡小龙", "雷文波", "唐一", "潘龙", "王飞", "杨芝建", "黄欢", "沈瑞", "严小华", "杨凡", "王小兵", "刘瑞林", "李安全", "张东", "刘少强", "曹烈彬", "袁浩", "何晓兵", "廖甫", "刘杨", "张清龙", "姚燕军", "艾国庆", "左飞", "沉香木", "尹老师", "郑仕勇", "张飞龙", "文国安", "小秦", "王海旭", "黎", "汪鑫", "应涛", "吴章敏", "马晓宏", "杨长江", "陈林", "梁兴刚", "邹绍祥", "王伯坚", "孔力", "龚钰", "牟小亮", "荣伟", "刘志民", "谭良全", "王小玉", "唐弟成", "陈维", "罗国举", "周宽", "杨明生", "向建", "陈跃", "陈元甲", "庄永坤", "谢李川", "凌昌波", "白邦琴", "杨 先生", "林小伟", "代培基", "黄东", "许明凤", "罗江祥", "方舰", "沈波", "周剑", "刘绿安", "吕卫", "马璐", "钱冲", "李德浩", "岳伦", "舒勇", "张胜强", "巫春来", "魏远波", "文玉坤", "唐祥", "钟永强", "文仲", "聂春", "李丰", "蒋鹏", "王刚", "王伟", "晏健", "杨友联", "唐强", "邱超", "王生义", "薛永户", "龙杰", "张立升", "康敏", "罗刚", "冯伦海", "罗星", "淦世福", "邓军", "周中余", "殷俊勇", "侯", "刘光辉", "徐章淋", "陈刚", "王希聪", "姜涛", "郭毅", "朱峰", "马先生", "刘豪", "周华", "吴硕", "李海霞", "雷强", "向伟", "罗云龙", "程家立", "冯德伟", "苏炀", "蔡卫", "谭均君", "刘毅", "刘超", "柯研", "车干", "唐友铜", "邓", "熊涛", "张", "毛源", "付德培", "王皓", "余建华", "胡金平", "王力", "周渝", "李勇", "周万里", "邓小勤", "陈渝童", "雷欢", "沈学兵", "艾晓东", "张小月", "胡智文", "吴杨", "赵行军", "陈彬", "张兵", "刘均", "石强", "余忠", "欧渝", "廖荣", "任民", "李波", "陈启民", "周寿忠", "陶小林", "童光余", "张玉福", "李老湿", "刘洋", "罗朋"],
        "patter_screen": ['杀价顾客', "不在预算内", "再过一段时间", "我要考虑一下"],
        "patter_content": ['某某爸爸（妈妈），我理解你的这种想法，一般我们为孩子选择早教最看重的就是这四件事： 1、课程的理念；2、老师的水平；3、卫生安全的环境；4、课程的价格。 但现实中，我从来没有见过一家机构能同时提供最好的理念、师资、环境、最低的价格给家长。 也就是这几项条件同时拥有的情况是不太可能的，就好比奔驰汽车不可能卖桑塔那的价格一样。 所以您现在要选择孩子的课程，你是愿意牺牲哪一项呢？愿意牺牲好的课程理念，师资，还是我们卫生安全的环境呢？ 所以有时候我们多投资一点，能得到孩子真正有益的东西还是蛮值得的，你说是吗？（我们什么时候开始给宝宝约课程呢？）',
            "当顾客（决策人）以他没有足够预算为借口，准备拖延成交或压价，怎么办？ 某某爸爸（妈妈），我完全理解您的想法，我有时也经常遇到预算外的，预算本来就有弹性的，是吗？您看这些课程对孩子今后的帮助，您会为了孩子，让预算制约了孩子的未来发展吗？",
            "当客户出现犹豫，以时间拖延决定的方式时。我们可以使用'鲍威尔'成交法。 某某爸爸（妈妈），您现在是觉得时间很多，再过一段时间，看到别的宝宝的变化，您可能就会后悔您考虑的这段时间耽误了孩子多少的成长机会，因为，孩子的成长只有一次机会，你忍心让孩子错过机会吗？",
            "某某爸爸（妈妈），您说要考虑一下，到底是哪方面呢？是对我们的课程、环境、还是服务还不了解呢？我能了解一下吗？或是，某某爸爸（妈妈），您是不是因为价格的问题吧？"],

        "activity_title": ["开学季", "视频投票大pk", "秋季续费体验班", "秋季多课程报名", "全民集赞"],
        "activity_type": ['抽奖', "拓客", "报名", "集赞", "分佣", "团购", "游戏", "投票", "砍价"],
        "class_type": ["多功能教室", "投影仪教室", "钢琴教室", "体育教室", "幼儿教室", "绘画教室", "多功能教室", "投影仪教室", "钢琴教室"],
        "class_id": ["301", "302", "303", "304", "305", "306", "307", "308", "309"],
        "class_date": ["", "", "", "", "", "", "", "", ""],
        "age": function () {
            return parseInt(Math.random() * 10)
        },
        "id": function () {
            return id += parseInt(Math.random() * 100);
        },
        "date": function () {
            var m = (parseInt(Math.random() * 12) + 1);
            if (m < 10) {
                m = "0" + m;
            }
            var day = (parseInt(Math.random() * 28) + 1);
            if (day < 10) {
                day = "0" + day;
            }
            return new Date().getFullYear() + "-" + m + "-" + day;
        }, "datetime": function () {
            var time = "";
            var h = (parseInt(Math.random() * 24) + 1);
            if (h < 10) {
                h = "0" + h;
            }
            var m = (parseInt(Math.random() * 60) + 1);
            if (m < 10) {
                m = "0" + m;
            }
            return MockData.date() + " " + h + ":" + m;
        }, random: function (min, max) {
            if (!max) {
                max = min;
                min = 1;
            }
            return parseInt(Math.random() * (max - min)) + min
        }
        , 'sex': [0, 1, 2],
        "phone": function () {
            var p = ["176", "159", "136", "157", "133", "189"];
            var d = parseInt((Math.random()) * 100000000);
            return p[parseInt(Math.random() * p.length)] + d;
        },
        "marketing_source": ["营销-集赞(六一特别活动)", "营销-集赞(感恩教师节)", "营销-抽奖(每日幸运儿)", "营销-表单(报名表单)"],
        "status_info": ['1', '2', '3', '4', '5', '6'],
        "admin_user": ['张老师', "李老师", "王老师", "李主管"],
        "marketing_follow_content": ["向父亲具体介绍了我们产品,有点心动", "说有空在看看,还需要想想", "每次都是说暂时没钱", "和对方交流过小孩早教的必要性", "对我们品牌还是比较认可的", "说已报名其他家的早教教育", "建议加强跟进,应该可以很快跟下来", "这个人不太好交流,可能需要放弃了", "对方孩子已经很大了,不适合我们的课程", "觉得我们的课程还是比较薄弱,已向他详细介绍过我们的课程"],
        "course_name": ['阿卡右脑潜能开发', "阿卡超级宝贝", "阿卡欢动音乐", "全能宝贝", "右脑潜能开发", "卡尔·奥尔夫", "蒙氏数理", "蒙氏感官", "超级宝贝", "欢动音乐"],
        "customer_tag": function (tag) {
            var children = MockData.customer_tags[tag - 1].children;
            var index = parseInt(Math.random() * children.length);
            return children[index].label;
        },
        "transfer_reason": ["离职", "调岗", "休假", "联系不上", "手机空号", "暂无意向"],
        "transfer_action": ["转让线索", "放弃线索"],
    };

    return {
        "mock": function (names) {
            names = names.split(',');
            var data = {};
            var me = this;
            _.each(names, function (name) {
                name = name.trim();
                var type = name;
                var params = "";
                if (name.indexOf(':') > 0) {
                    var is = name.split(':');
                    name = is[0].trim();
                    type = is[1].trim();
                    if (type.indexOf('|') > 0) {
                        var type_params = type;
                        is = type.split('|');
                        type = is[0].trim();
                        params = type_params.substring(type.length + 1);
                        params = params.replace('|', ',');
                    }
                }
                var mock = MockData[type];
                if (!mock) {
                    mock = me[type];
                    if (!mock) {
                        return;
                    }
                }
                if (_.isFunction(mock)) {
                    data[name] = eval('mock(' + params + ')');
                } else {
                    var index = 0;
                    if (MockData[type]) {
                        index = parseInt(Math.random() * mock.length);
                    } else {
                        index = parseInt(Math.random() * (mock.length - 1)) + 1;
                    }
                    var item = mock[index];
                    if (_.isObject(item)) {
                        if (!params) {
                            params = 'name';
                            item = item[na];
                        }
                    }
                    data[name] = item;
                }
            });
            return data;
        }, mockList: function (names, size) {
            if (!size) {
                size = 20;
            }
            var list = [];
            for (var i = 0; i < size; i++) {
                var data = this.mock(names);
                list.push(data);
            }
            return list;
        }, search: function (store, key) {
            return function (key, value) {
                store.search({key: value});
            }
        }
    };

});